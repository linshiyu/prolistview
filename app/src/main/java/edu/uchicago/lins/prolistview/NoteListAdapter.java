package edu.uchicago.lins.prolistview;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

public class NoteListAdapter extends RecyclerView.Adapter<NoteListAdapter.NoteViewHolder> {

    class NoteViewHolder extends RecyclerView.ViewHolder {
        private final TextView noteItemView;
        private LinearLayout linearLayout;

        private NoteViewHolder(View itemView) {
            super(itemView);
            noteItemView = itemView.findViewById(R.id.textView);
            linearLayout = itemView.findViewById(R.id.LinearLayout);
        }
    }

    private final LayoutInflater mInflater;
    private List<Note> mNotes; // Cached copy of words
    private Context context;

    NoteListAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public NoteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.recyclerview_item, parent, false);
        return new NoteViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(NoteViewHolder holder, int position) {
        if (mNotes != null) {
            final Note current = mNotes.get(position);
            holder.noteItemView.setText(current.getName());
            holder.linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ViewEditActivity.class);
                    intent.putExtra("name", current.getName());
                    intent.putExtra("text", current.getText());
                    context.startActivity(intent);
                }
            });
        } else {
            // Covers the case of data not being ready yet.
            holder.noteItemView.setText("No Note");
        }


    }

    void setNotes(List<Note> notes){
        mNotes = notes;
        notifyDataSetChanged();
    }

    // getItemCount() is called many times, and when it is first called,
    // mWords has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (mNotes != null)
            return mNotes.size();
        else return 0;
    }


    public Note getWordAtPosition (int position) {
        return mNotes.get(position);
    }
}