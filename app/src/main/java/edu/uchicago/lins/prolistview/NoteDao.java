package edu.uchicago.lins.prolistview;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface NoteDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insert(Note note);

    @Query("DELETE FROM note_table")
    public void deleteAll();

    @Query("SELECT * from note_table ORDER BY note ASC")
    public LiveData<List<Note>> getAllNotes();

    @Delete
    public void delete(Note note);

    @Query("UPDATE note_table SET text = :new_text WHERE note = :name")
    public void update(String name, String new_text);

    @Delete
    void deleteNote(Note note);

}
