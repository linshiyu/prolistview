package edu.uchicago.lins.prolistview;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class ViewEditActivity extends AppCompatActivity {
    private NoteViewModel mNoteViewModel;
    private String name;
    private String text;
    private TextView mainText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_edit);
        mNoteViewModel = ViewModelProviders.of(this).get(NoteViewModel.class);
        Intent intent = getIntent();
        name = intent.getStringExtra("name");
        text = intent.getStringExtra("text");
        mainText = findViewById(R.id.mainText);
        mainText.setText(text);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //menu quit
        if (id == R.id.Quit) {
            finish();
            return true;
        }

        //menu edit
        if (id == R.id.Edit){
            Intent intent = new Intent(ViewEditActivity.this, EditActivity.class);
            intent.putExtra("name", name);
            intent.putExtra("text", text);
            startActivity(intent);
            finish();
            return true;
        }

        if (id == R.id.Delete){
            mNoteViewModel.deleteNote(new Note(name, text));
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
