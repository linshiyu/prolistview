package edu.uchicago.lins.prolistview;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "note_table")
public class Note {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "note")
    private String name;

    @NonNull
    private String text;

    public Note(@NonNull String name, @NonNull String text){
        this.name = name;
        this.text = text;
    }

    public String getName(){
        return name;
    }

    public String getText(){
        return text;
    }

    public void setName(String newName){
        this.name = newName;
    }

    public void setText(String newText){
        this.text = newText;
    }
}
