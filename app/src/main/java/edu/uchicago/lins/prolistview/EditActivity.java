package edu.uchicago.lins.prolistview;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class EditActivity extends AppCompatActivity {
    private NoteViewModel mNoteViewModel;
    private EditText editText;
    String name;
    String text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        editText = findViewById(R.id.edit_text);
        Intent intent = getIntent();
        name = intent.getStringExtra("name");
        text = intent.getStringExtra("text");
        editText.setText(text);
        mNoteViewModel = ViewModelProviders.of(this).get(NoteViewModel.class);

        final Button button = findViewById(R.id.button_save);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent replyIntent = new Intent();
                if (TextUtils.isEmpty(editText.getText())) {
                    setResult(RESULT_CANCELED, replyIntent);
                } else {
                    String t = editText.getText().toString();
                    mNoteViewModel.update(new Note(name, t));
                    setResult(RESULT_OK, replyIntent);
                }
                finish();
            }
        });
    }
}
