package edu.uchicago.lins.prolistview;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import java.util.List;

public class NoteViewModel extends AndroidViewModel {

    private NoteRepository mRepository;
    private LiveData<List<Note>> mAllNotes;

    public NoteViewModel (Application application) {
        super(application);
        mRepository = new NoteRepository(application);
        mAllNotes = mRepository.getAllNotes();
    }

    LiveData<List<Note>> getAllNotes() {
        return mAllNotes;
    }

    public void insert(Note note) { mRepository.insert(note); }

    public void deleteAll() {mRepository.deleteAll();}

    public void deleteNote(Note note) {mRepository.deleteNote(note);}

    public void update(Note note) { mRepository.update(note);}
}
